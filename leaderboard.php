<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Manyosu! - A custom osu! server">
    <meta name="author" content="">

    <title>Manyosu!</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <style>
    body {
        padding-top: 70px;
        /* Required padding for .navbar-fixed-top. Remove if using .navbar-static-top. Change if height of navigation changes. */
    }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Manyosu!</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <?php include 'includes/navbar.php' ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-lg-12 text-center">
              <?php

                require_once "web/inc/functions_db.php";

                $db = sqlconn();
                $query = "SELECT DISTINCT playerID FROM scores";
                $data = $db->query($query);

                foreach($data as $row) {
                    $column[] = $row['playerID'];
                  }

                  echo '<table class="table table-bordered">';
                  echo '<thead>';
                  echo '<tr>';
                  echo
                  "<th>Rank</th>".
                  "<th>Username</th>".
                  "<th>Total score</th>".
                  "<th>Level</th>".
                  "<th>Total plays</th>";
                  echo "</tr>";
                  echo "</thead>";

                  //make arrays
                  $playerIDs = array();
                  $scores = array();
                  $plays = array();

                  //turn into array
                  foreach ($column as $playerID) {
                    $playerIDs[] = $playerID;
                    $scores[] = getTotalScore($playerID);
                    $plays[] = getPlays($playerID);
                  //moar stuffs here
                }

                //sort
                array_multisort($scores, SORT_DESC, $playerIDs, $plays);
                //array_multisort($scores, SORT_DESC, $plays);

                  //display stuff
                  $i = 0;
                  while ($i < count($playerIDs)) {
                    $rank = $i + 1;
                    $playerID = $playerIDs[$i];
                    if (!isUserBanned($playerID)) {
  						    	echo "<tbody>";
  									echo "<tr>";
  							    echo '<td rowspan="2">'.$rank."</td>";
  									echo '<td>'.getUserName($playerID)."</a></td>";
  									echo "<td>".number_format($scores[$i])."</td>";
  									echo "<td>".getLevel($scores[$i])."</td>";
  									echo "<td>".number_format($plays[$i])."</td>";
  									echo "</tr>";

                    //doesnt properly work yet.
                    //actually it does, nevermind the text above me
                    }
                    $i++;
                  }
                ?>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
