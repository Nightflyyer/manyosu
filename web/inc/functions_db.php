<?php
/*
require_once '/home/osuserve/public_html/web/inc/functions_db.php';
*/
function sqlconn() {
	require 'sqlconn.php';
	return $db;
}

function isUserBanned($playerID) {
	$db = sqlconn();
	$ret = true;

	$query = 'SELECT banned FROM users WHERE playerID = :player LIMIT 1';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':player', $playerID, PDO::PARAM_INT);
	$prepared->execute();
	$result = $prepared->fetch();
	$result = $result[0];

	if ($result == 0) {
		$ret = false;
	}
	return $ret;
}

function getAvatarID($playerID) {
	$db = sqlconn();
	$query = 'SELECT avatarID FROM users WHERE playerID = :user LIMIT 1';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
	$prepared->execute();
	$result = $prepared->fetch();
	$result = $result[0];

	//cookiezi: 124493
	//me: 2287881
	//lewa: 475021
	//WWW: 39828
	//peppy: 2
	//bancho: 3

	if ($result == '' || $result == '0') {
		$result = -1;
	}

	return $result;
}

function getPlayerID($username) {
	$db = sqlconn();
	$query = 'SELECT playerID FROM users WHERE username = :username LIMIT 1';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':username', $username, PDO::PARAM_STR);
	$succes = $prepared->execute();
	if ($succes) {
		$result = $prepared->fetchColumn();
		return $result;
	} else {
		return 'Query failed';
	}
}

function getUserName($playerID) {
	$db = sqlconn();
	$query = 'SELECT username FROM users WHERE playerID = :user LIMIT 1';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
	$prepared->execute();
	$result = $prepared->fetch();
	$result = $result[0];

	if ($result == '') {
		$result = $playerID;
	}

	return $result;
}

function getAccuracy($playerID) {
	$db = sqlconn();
	$prepared = $db->prepare('SELECT (SUM(count300)*6 + SUM(count100)*2 + SUM(count50)*1)/((SUM(count300)+SUM(count100)+SUM(count50)+SUM(countMiss))*6) FROM scores WHERE playerID = :user');
  $prepared->bindParam(':user', $playerID, PDO::PARAM_STR);
  $prepared->execute();

  return $prepared->fetch()[0];
}

function getRank($playerID) {
	$db = sqlconn();
	$query = "SELECT DISTINCT playerID FROM scores";
	$data = $db->query($query);
	foreach($data as $row) {
			$column[] = $row['playerID'];
		}
		$playerIDs = array();
		$scores = array();
		$plays = array();
		foreach ($column as $playerID) {
			$playerIDs[] = $playerID;
			$scores[] = getTotalScore($playerID);
			$plays[] = getPlays($playerID);
	}
	array_multisort($scores, SORT_DESC, $playerIDs, $plays);
	$i = 0;
	while ($i < count($playerIDs)) {
		$rank = $i + 1;
		$playerID = $playerIDs[$i];
		return $rank;
		}
		$i++;
}

function getExp($lvl) {
    return $xp = pow($lvl, 2) + 5 * $lvl;
}

function doActivateUser($playerID) {
	$db = sqlconn();
  $query = 'UPDATE users SET status = 1 WHERE playerID = :user';

  $prepared = $db->prepare($query);
  $prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
  return $prepared->execute();
}

function doBanUser($playerID) {
	$db = sqlconn();
  $query = 'UPDATE users SET status = -1 WHERE playerID = :user';

  $prepared = $db->prepare($query);
  $prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
  return $prepared->execute();
}

function getTotalScore($playerID) {
	$db = sqlconn();
	$query = 'SELECT SUM(score) FROM scores WHERE playerID = :user';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
	$prepared->execute();

	$totalScore = $prepared->fetch();
	return $totalScore[0];
}

function getStatus($playerID) {
	$db = sqlconn();
	$query = 'SELECT status FROM users WHERE playerID = :user LIMIT 1';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
	$prepared->execute();

	$totalScore = $prepared->fetch();
	return $totalScore[0];
}

function getTotalScoreForMode($playerID, $mode) {
	$db = sqlconn();
	$query = 'SELECT SUM(score) FROM scores WHERE playerID = :user AND mode = :mode';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':user', $playerID, PDO::PARAM_INT);
	$prepared->bindParam(':mode', $mode, PDO::PARAM_INT);
	$prepared->execute();

	$totalScore = $prepared->fetch();
	return $totalScore[0];
}

function getPlays($playerID) {
	$db = sqlconn();
	$query = 'SELECT replayID FROM scores WHERE playerID = :playerID';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':playerID', $playerID, PDO::PARAM_INT);
	$prepared->execute();
	$rows = $prepared->fetchAll();
	$rowCount = count($rows);
	return $rowCount;
}

function getPlaysForMode($playerID, $mode) {
	$db = sqlconn();
	$query = 'SELECT replayID FROM scores WHERE playerID = :playerID AND mode = :mode';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':playerID', $playerID, PDO::PARAM_INT);
	$prepared->bindParam(':mode', $mode, PDO::PARAM_INT);
	$prepared->execute();
	$rows = $prepared->fetchAll();
	$rowCount = count($rows);
	return $rowCount;
}

function addAccount($username, $password) {
	$password = md5($password);
	$db = sqlconn();

	//if user already exists
	if (getPlayerID($username) != '') {
		echo "Already in db<br>";
		return false;
	}

	$query = 'INSERT INTO users (username)
			VALUES (:username)';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':username', $username, PDO::PARAM_STR);
	echo ($prepared->execute() ? '<div class="alert alert-success" role="alert"><center>Registered!</center></div>' : '<div class="alert alert-danger" role="alert"><center>Registered!</center></div>');


	$salt = substr(md5(rand()), 0, 5);
	$prepared = $db->prepare('UPDATE users SET passwordHash = :passhash, salt = :salt WHERE username = :username');
	$prepared->bindParam(':username', $username, PDO::PARAM_STR);
	$prepared->bindParam(':passhash', hashPassword($password, $salt), PDO::PARAM_STR);
	$prepared->bindParam(':salt', $salt, PDO::PARAM_STR);	//random string
	return $prepared->execute();

	//return checkGlobalLogin($username, $password);
}

function checkLogin($user, $pass) {
	//make pass more secure
	$db = sqlconn();
	$query = 'SELECT passwordHash, salt, playerID FROM users WHERE username = :user LIMIT 1';
	$prepared = $db->prepare($query);
	$prepared->bindParam(':user', $user, PDO::PARAM_STR);
	$prepared->execute();
	$result = $prepared->fetchAll();
	$count = count($result);

	if ($count != 0) {
		$row = $result[0];
		$passHash = $row['passwordHash'];
		$salt = $row['salt'];

		if ($passHash == '') {
			//password not in db yet
			$db = sqlconn();

			$salt = substr(md5(rand()), 0, 5);

			$prepared = $db->prepare('UPDATE users SET passwordHash = :passhash, salt = :salt WHERE username = :user');
			$prepared->bindParam(':user', $user, PDO::PARAM_STR);
			$prepared->bindParam(':passhash', hashPassword($pass, $salt), PDO::PARAM_STR);
			$prepared->bindParam(':salt', $salt, PDO::PARAM_STR);	//random string
			$prepared->execute();

			return ($prepared->rowCount() == 1);
		}

		if (hashPassword($pass, $salt) == $passHash)
			return true;

	} else {
		//echo 'User not in database.</br>';
		//user not in database
	}
	return false;
}

function hashPassword($pass, $salt) {
	return md5(md5($pass).$salt);
}

function generateRandomString($length = 10) {	//the magic of the internet :)
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getLevel($n)
{
	for ($i=0; $i < 100; $i++) {
		$scoreToBeat = (5000 / 3) * (4*pow($i, 3) - 3*pow($i, 2) - $i) + 1.25 * pow(1.8, ($i - 60));
		if ($n < $scoreToBeat) {
			return $i;
		}
	}
	return "level too high :c";
}
?>
